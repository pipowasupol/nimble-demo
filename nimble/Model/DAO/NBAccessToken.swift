//
//  NBAccessToken.swift
//  nimble
//
//  Created by Wasupol Tungsakultong on 2/23/2560 BE.
//  Copyright © 2560 Wasupol Tungsakultong. All rights reserved.
//

import Foundation
import ObjectMapper

//{"access_token":"d9584af77d8c0d6622e2b3c554ed520b2ae64ba0721e52daa12d6eaa5e5cdd93","token_type":"bearer","expires_in":7200,"created_at":1485174186}

class NBAccessToken: Mappable {
    var accessToken = ""
    var tokenType = ""
    var expiresIn = 0
    var createAt: TimeInterval = 0
    
    required init?(map: Map) {
        // required by api
    }
    
    func mapping(map: Map) {
        accessToken <- map["access_token"]
        tokenType <- map["token_type"]
        expiresIn <- map["expires_in"]
        createAt <- map["created_at"]
    }
}
