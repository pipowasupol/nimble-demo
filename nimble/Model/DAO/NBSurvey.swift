//
//  NBSurvey.swift
//  nimble
//
//  Created by Wasupol Tungsakultong on 2/23/2560 BE.
//  Copyright © 2560 Wasupol Tungsakultong. All rights reserved.
//

import Foundation
import ObjectMapper

class NBSurvey: Mappable {
    
    var surveyId = ""
    var surveyTitle: String? = ""
    var surveyDescription: String? = ""
    var accessCodePrompt: String? = nil
    var thankEmailAboveThreshold: String? = nil
    var thankEmailBelowThreshold: String? = nil
    var footerContent: String? = nil
    var isActive = false
    var coverImageURL: URL? = nil
    var coverBackgroundColor: UIColor? = nil
    var type: String? = ""
    var createdAt: Date? = nil
    var activeAt: Date? = nil
    var inActiveAt: Date? = nil
    var surveyVersion = 0
    var shortUrl: String? = nil
    var languageList: [String]? = nil
    var defaultLanguage: String? = nil
    var tagList: String? = nil
    var isAccessCodeRequired = false
    var isAccessCodeValidRequired = false
    var accessCodeValidation: String? = nil
    var theme: NSSurveyTheme? = nil
    var questions: [NSSurveyQuestion]? = nil
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        surveyId <- map["id"]
        surveyTitle <- map["title"]
        surveyDescription <- map["description"]
        accessCodePrompt <- map["access_code_prompt"]
        thankEmailAboveThreshold <- map["thank_email_above_threshold"]
        thankEmailBelowThreshold <- map["thank_email_below_threshold"]
        footerContent <- map["footer_content"]
        isActive <- map["is_active"]
        coverImageURL <- (map["cover_image_url"], URLTransform(shouldEncodeURLString: false))
        coverBackgroundColor <- (map["cover_background_color"], HexColorTransform(prefixToJSON: true, alphaToJSON: false))
        type <- map["type"]
        createdAt <- (map["created_at"], CustomDateFormatTransform(formatString: kDefaultDateFormat))
        activeAt <- (map["active_at"], CustomDateFormatTransform(formatString: kDefaultDateFormat))
        inActiveAt <- (map["inactive_at"], CustomDateFormatTransform(formatString: kDefaultDateFormat))
        surveyVersion <- map["survey_version"]
        shortUrl <- map["short_url"]
        languageList <- map["language_list"]
        defaultLanguage <- map["default_language"]
        tagList <- map["tag_list"]
        isAccessCodeRequired <- map["is_access_code_required"]
        isAccessCodeValidRequired <- map["is_access_code_valid_required"]
        accessCodeValidation <- map["access_code_validation"]
        theme <- map["theme"]
        questions <- map["questions"]
    }
}

class NSSurveyTheme: Mappable {
    var colorActive: UIColor? = nil
    var colorInActive: UIColor? = nil
    var colorQuestion: UIColor? = nil
    var colorAnswerNormal: UIColor? = nil
    var colorAnswerInactive: UIColor? = nil
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        colorActive <- (map["color_active"], HexColorTransform(prefixToJSON: true, alphaToJSON: false))
        colorInActive <- (map["color_inactive"], HexColorTransform(prefixToJSON: true, alphaToJSON: false))
        colorQuestion <- (map["color_question"], HexColorTransform(prefixToJSON: true, alphaToJSON: false))
        colorAnswerNormal <- (map["color_answer_normal"], HexColorTransform(prefixToJSON: true, alphaToJSON: false))
        colorAnswerInactive <- (map["color_answer_inactive"], HexColorTransform(prefixToJSON: true, alphaToJSON: false))
    }
}

class NSSurveyQuestion: Mappable {
    
    var questionId = ""
    var text: String? = nil
    var helpText: String? = nil
    var displayOrder = 0
    var shortText: String? = nil
    var pick: String? = nil
    var displayType: String? = nil
    var isMandatory = false
    var correctAnswerId: String? = nil
    var facebookProfile: String? = nil
    var twitterProfile: String? = nil
    var imageURL: URL? = nil
    var coverImageURL: URL? = nil
    var coverImageOpacity: CGFloat? = 0
    var coverBackgroundColor: UIColor? = nil
    var isShareableOnFacebook = false
    var isShareableOnTwitter = false
    var fontFace: String? = nil
    var fontSize: CGFloat? = 0
    var tagList: String? = nil
    var answers: [NSSurveyAnswer]? = nil
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        questionId <- map["id"]
        text <- map["text"]
        helpText <- map["help_text"]
        displayOrder <- map["display_order"]
        shortText <- map["short_text"]
        pick <- map["pick"]
        displayType <- map["display_type"]
        isMandatory <- map["is_mandatory"]
        correctAnswerId <- map["correct_answer_id"]
        facebookProfile <- map["facebook_profile"]
        twitterProfile <- map["twitter_profile"]
        imageURL <- (map["image_url"], URLTransform(shouldEncodeURLString: false))
        coverImageURL <- (map["cover_image_url"], URLTransform(shouldEncodeURLString: false))
        coverImageOpacity <- map["cover_image_opacity"]
        coverBackgroundColor <- (map["cover_background_color"], HexColorTransform(prefixToJSON: true, alphaToJSON: false))
        isShareableOnFacebook <- map["is_shareable_on_facebook"]
        isShareableOnTwitter <- map["is_shareable_on_twitter"]
        fontFace <- map["font_face"]
        fontSize <- map["font_size"]
        tagList <- map["tag_list"]
        answers <- map["answers"]
    }
}

//{
//    id: "4cbc3e5a1c87d99bc7ee",
//    question_id: "940d229e4cd87cd1e202",
//    text: "1",
//    help_text: null,
//    input_mask_placeholder: null,
//    short_text: "answer_1",
//    is_mandatory: false,
//    is_customer_first_name: false,
//    is_customer_last_name: false,
//    is_customer_title: false,
//    is_customer_email: false,
//    prompt_custom_answer: false,
//    weight: null,
//    display_order: 0,
//    display_type: "default",
//    input_mask: null,
//    date_constraint: null,
//    default_value: null,
//    response_class: "answer",
//    reference_identifier: null,
//    score: 0,
//    alerts: [ ]
//}

class NSSurveyAnswer: Mappable {
    
    var answerId = ""
    var questionId = ""
    var text: String? = nil
    var helpText: String? = nil
    var inputMaskPlaceholder: String? = nil
    var shortText: String? = nil
    var isMandatory = false
    var isCustomerFirstName = false
    var isCustomerLastName = false
    var isCustomerTitle = false
    var isCustomerEmail = false
    var shouldPromptCustomAnswer = false
    var weight: String? = nil
    var displayOrder = 0
    var displayType: String? = nil
    var inputMask: String? = nil
    var dateConstraint: Date? = nil
    var defaultValue: String? = nil
    var responseClass: String? = nil
    var referenceIdentifier: String? = nil
    var score = 0
    var alerts: [String]? = nil
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        answerId <- map["id"]
        questionId <- map["question_id"]
        text <- map["text"]
        helpText <- map["help_text"]
        inputMaskPlaceholder <- map["input_mask_placeholder"]
        shortText <- map["short_text"]
        isMandatory <- map["is_mandatory"]
        isCustomerFirstName <- map["is_customer_first_name"]
        isCustomerLastName <- map["is_customer_last_name"]
        isCustomerTitle <- map["is_customer_title"]
        isCustomerEmail <- map["is_customer_email"]
        shouldPromptCustomAnswer <- map["prompt_custom_answer"]
        weight <- map["weight"]
        displayOrder <- map["display_order"]
        displayType <- map["display_type"]
        inputMask <- map["input_mask"]
        dateConstraint <- (map["date_constraint"], CustomDateFormatTransform(formatString: kDefaultDateFormat))
        responseClass <- map["response_class"]
        referenceIdentifier <- map["reference_identifier"]
        score <- map["score"]
        alerts <- map["alerts"]
    }
}
