//
//  NBRequest.swift
//  nimble
//
//  Created by Wasupol Tungsakultong on 2/22/2560 BE.
//  Copyright © 2560 Wasupol Tungsakultong. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class NBRequest <T: Mappable> {
    typealias CompletionBlock_Object = (DataResponse<T>) -> Void
    typealias CompletionBlock_Array = (DataResponse<[T]>) -> Void
    typealias CompletionBlock_JSON = (DataResponse<Any>) -> Void
    
    private var completionBlock_Object: CompletionBlock_Object?
    private var completionBlock_Array: CompletionBlock_Array?
    private var completionBlock_JSON: CompletionBlock_JSON?
    
    var keyPath: String?
    var urlRequest: String!
    var params: [String : Any]?
    var paramsEncode: ParameterEncoding = JSONEncoding.default
    var header: [String : String]?
    var method: Alamofire.HTTPMethod = .get
    
    var request: DataRequest?
    
    init(method: Alamofire.HTTPMethod, urlRequest: String!, keyPath: String?, params: [String : Any]?, paramsEncodeing: ParameterEncoding ,header: [String : String]?) {
        self.urlRequest = urlRequest
        self.keyPath = keyPath
        self.params = params
        self.header = header
        self.method = method
        self.paramsEncode = paramsEncodeing
    }
    
    // response object
    func drawerObjectCompletionHandler(completionHandler: @escaping CompletionBlock_Object) -> () {
        self.completionBlock_Object = completionHandler
    }
    
    private func startResponseObject(_ completionHandler: CompletionBlock_Object) -> () {
        let currentRequest = Alamofire.request(self.urlRequest, method: method, parameters: params, encoding: paramsEncode, headers: header)
        currentRequest.responseObject { (completionHandler: DataResponse<T>) in
            if completionHandler.response?.statusCode == 401 {
                //Refresh token
                self.checkRefreshTokenIfNeed()
                return
            }
            self.completionBlock_Object?(completionHandler)
        }
        self.request = currentRequest
    }
    
    func drawerArrayCompletionHandler(completionHandler: @escaping CompletionBlock_Array) -> () {
        self.completionBlock_Array = completionHandler
    }
    
    private func startResponseArray(_ completionHandler: CompletionBlock_Array) -> () {
        let currentRequest = Alamofire.request(urlRequest, method: method, parameters: params, encoding: paramsEncode, headers: header)
        currentRequest.responseArray { (completionHandler: DataResponse<[T]>) in
            if completionHandler.response?.statusCode == 401 {
                //refresh token
                self.checkRefreshTokenIfNeed()
                return
            }
            self.completionBlock_Array?(completionHandler)
        }
        self.request = currentRequest
    }
    
    func drawerJSONCompletionHandler(completionHandler: @escaping CompletionBlock_JSON) -> () {
        self.completionBlock_JSON = completionHandler
    }
    
    private func startResponseJSON(_ completionHandler: CompletionBlock_JSON) {
        let currentRequest = Alamofire.request(urlRequest, method: method, parameters: params, encoding: paramsEncode, headers: header)
        currentRequest.responseJSON { (completionHandler :DataResponse<Any>) in
            if completionHandler.response?.statusCode == 401 {
                //refresh token
                self.checkRefreshTokenIfNeed()
                return
            }
            self.completionBlock_JSON?(completionHandler)
        }
        self.request = currentRequest
    }
    
    // Control
    
    func start() {
        if completionBlock_Object != nil {
            startResponseObject(completionBlock_Object!)
        } else if completionBlock_Array != nil {
            startResponseArray(completionBlock_Array!)
        } else if completionBlock_JSON != nil {
            startResponseJSON(completionBlock_JSON!)
        }
    }
    
    func cancel() {
        self.request?.cancel()
    }
    
    func pause() {
        self.request?.suspend()
    }
    
    func resume(){
        self.request?.resume()
    }
    
    func checkRefreshTokenIfNeed() {
        NBAppGlobal.sharedInstance.refreshToken { (success) in
            if success {
                self.refreshHeaderToken()
            }
            self.start()
        }
    }
    
    func refreshHeaderToken() {
        guard let accessToken = NBAppGlobal.sharedInstance.service.accessToken else { return }
        if !accessToken.isEmpty {
            self.params?["access_token"] = accessToken
        }
    }
    
    // dealoc
    deinit {
        self.cancel()
        self.request = nil
        if completionBlock_Object != nil {
            self.completionBlock_Object = nil
        } else if completionBlock_Array != nil {
            self.completionBlock_Array = nil
        } else if completionBlock_JSON != nil {
            self.completionBlock_JSON = nil
        }
    }
}
