//
//  NBAppGlobal.swift
//  nimble
//
//  Created by Wasupol Tungsakultong on 2/23/2560 BE.
//  Copyright © 2560 Wasupol Tungsakultong. All rights reserved.
//

import Foundation
import KeychainSwift

let kNimbleURLService = "https://nimbl3-survey-api.herokuapp.com/"
let kDefaultDateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"

class NBAppGlobal {
    static let sharedInstance = NBAppGlobal()
    
    let service: NBService!
    private var keychain: KeychainSwift!
    private var refreshTokenRequest: NBRequest<NBAccessToken>?
    
    init() {
        self.service = NBService(baseURLString: kNimbleURLService)
        self.keychain = KeychainSwift()
        self.tryToLoadAccessToken()
    }
    
    func saveAccessToken() {
        guard let accessToken = self.service.accessToken else { return }
        let oldAccessToken = keychain.get("access_token")
        if accessToken != oldAccessToken {
            self.clearAccessToken()
            if !accessToken.isEmpty {
                keychain.set(accessToken, forKey: "access_token")
            }
        }
    }
    
    func refreshToken(_ completionHandler: ((Bool) -> Void)?) {
        if refreshTokenRequest == nil {
            self.refreshTokenRequest = NBAppGlobal.sharedInstance.service.requestForRefreshOAuthToken()
            refreshTokenRequest?.drawerObjectCompletionHandler(completionHandler: { (response) in
                self.refreshTokenRequest = nil
                if response.result.isSuccess, let accessToken = response.result.value {
                    self.service.accessToken = accessToken.accessToken
                    self.saveAccessToken()
                    completionHandler?(true)
                } else {
                    completionHandler?(false)
                }
            })
            refreshTokenRequest?.start()
        }
    }
    
    private func tryToLoadAccessToken() {
        guard let accessToken = keychain.get("access_token") else { return }
        self.service.accessToken = accessToken
    }
    
    private func clearAccessToken() {
        keychain.delete("access_token")
    }
}
