//
//  NBBaseService.swift
//  nimble
//
//  Created by Wasupol Tungsakultong on 2/23/2560 BE.
//  Copyright © 2560 Wasupol Tungsakultong. All rights reserved.
//

import Foundation
import Alamofire

class NBBaseService {
    var baseURL: String!
    var headers: [String : String]!
    var accessToken: String?
    
    init(baseURLString: String) {
        self.baseURL = baseURLString
    }
}

class NBService: NBBaseService {
    func requestForRefreshOAuthToken() -> NBRequest<NBAccessToken> {
        let URLString = kNimbleURLService + "oauth/token"
        let params = [
            "grant_type": "password",
            "username": "carlos@nimbl3.com",
            "password": "antikera"
        ]
        return NBRequest<NBAccessToken>(method: .post, urlRequest: URLString, keyPath: nil, params: params, paramsEncodeing: JSONEncoding.default, header: self.headers)
    }
    
    func requestForSurveyAPI(page: Int?, per_page: Int?) -> NBRequest<NBSurvey> {
        let URLString = kNimbleURLService + "surveys.json"
        var params: [String: Any] = [:]
        if page != nil, per_page != nil {
            params["page"] = page!
            params["per_page"] = per_page!
        }
        if let accessToken = accessToken, !accessToken.isEmpty {
            params["access_token"] = accessToken
        }
        return NBRequest<NBSurvey>(method: .get, urlRequest: URLString, keyPath: nil, params: params, paramsEncodeing: URLEncoding.default, header: self.headers)
    }
}
