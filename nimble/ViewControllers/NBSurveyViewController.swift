//
//  NBSurveyViewController.swift
//  nimble
//
//  Created by Wasupol Tungsakultong on 2/27/2560 BE.
//  Copyright © 2560 Wasupol Tungsakultong. All rights reserved.
//

import UIKit

class NBSurveyViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var takeSurvey: UIButton!
    @IBOutlet weak var surveyImageView: UIImageView!
    
    var survey: NBSurvey!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = survey.coverBackgroundColor ?? UIColor(hex: "#333333")
        self.nameLabel.text = survey.surveyTitle
        self.subTitleLabel.text = survey.surveyDescription
        self.takeSurvey.layer.cornerRadius = self.takeSurvey.frame.height / 2
        if var coverImageURL = survey.coverImageURL?.absoluteString {
            coverImageURL += "l"
            ImageNetworking.sharedLoader.imageForUrl(urlString: coverImageURL) { (image, url) in
                if image != nil {
                    self.surveyImageView.image = image
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
