//
//  NBMainSurveysViewController.swift
//  nimble
//
//  Created by Wasupol Tungsakultong on 2/25/2560 BE.
//  Copyright © 2560 Wasupol Tungsakultong. All rights reserved.
//

import UIKit

let perPage = 10

class NBMainSurveysViewController: UIViewController, NBSurveysPageViewControllerDelegate {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var containerView: UIView!
    
    var surveysPageViewController: NBSurveysPageViewController? {
        didSet {
            surveysPageViewController?.surveysPageViewControllerDelegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        pageControl.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI_2))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        ImageNetworking.sharedLoader.cache.removeAllObjects()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let surveysPageViewController = segue.destination as? NBSurveysPageViewController {
            self.surveysPageViewController = surveysPageViewController
        }
    }
    

    @IBAction func refreshButtonTouchUp(_ sender: UIBarButtonItem) {
        self.surveysPageViewController?.page = 0
        self.surveysPageViewController?.surveys?.removeAll()
        self.surveysPageViewController?.loadSurveysIfNeed()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func surveysPageViewController(surveysPageViewController: NBSurveysPageViewController, didUpdatePageCount count: Int) {
        pageControl.numberOfPages = count
    }
    
    func surveysPageViewController(surveysPageViewController: NBSurveysPageViewController, didUpdatePageIndex index: Int) {
        pageControl.currentPage = index
        if index == pageControl.numberOfPages - 1 {
            // last index
            self.surveysPageViewController?.loadSurveysIfNeed()
        }
    }
}
