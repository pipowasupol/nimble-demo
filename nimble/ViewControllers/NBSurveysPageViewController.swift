//
//  NBSurveysPageViewController.swift
//  nimble
//
//  Created by Wasupol Tungsakultong on 2/27/2560 BE.
//  Copyright © 2560 Wasupol Tungsakultong. All rights reserved.
//

import UIKit
import PKHUD

protocol NBSurveysPageViewControllerDelegate: class {
    /**
     Called when the number of pages is updated.
     
     - parameter surveysPageViewController: the SurveysPageViewController instance
     - parameter count: the total number of pages.
     */
    func surveysPageViewController(surveysPageViewController: NBSurveysPageViewController,
                                    didUpdatePageCount count: Int)
    
    /**
     Called when the current index is updated.
     
     - parameter surveysPageViewController: the SurveysPageViewController instance
     - parameter index: the index of the currently visible page.
     */
    func surveysPageViewController(surveysPageViewController: NBSurveysPageViewController,
                                    didUpdatePageIndex index: Int)
}

class NBSurveysPageViewController: UIPageViewController {

    weak var surveysPageViewControllerDelegate: NBSurveysPageViewControllerDelegate?
    
    var orderedViewControllers: [NBSurveyViewController]!
    
    private var surveyRequest: NBRequest<NBSurvey>?
    var surveys: [NBSurvey]?
    var page = 1
    private var lastIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        delegate = self
        dataSource = self
        
        loadSurveysIfNeed()
    }

    /**
     Scrolls to the view controller at the given index. Automatically calculates
     the direction.
     
     - parameter newIndex: the new index to scroll to
     */
    func scrollToViewController(index newIndex: Int) {
        if let firstViewController = viewControllers?.first as? NBSurveyViewController,
            let currentIndex = orderedViewControllers.index(of: firstViewController) {
            let direction: UIPageViewControllerNavigationDirection = newIndex >= currentIndex ? .forward : .reverse
            let nextViewController = orderedViewControllers[newIndex]
            scrollToViewController(nextViewController, direction: direction)
        }
    }
    
    /**
     Scrolls to the next view controller.
     */
    func scrollToNextViewController() {
        if let visibleViewController = viewControllers?.first,
            let nextViewController = pageViewController(self, viewControllerAfter: visibleViewController) {
            scrollToViewController(nextViewController)
        }
    }
    
    /**
     Scrolls to the given 'viewController' page.
     
     - parameter viewController: the view controller to show.
     */
    private func scrollToViewController(_ viewController: UIViewController,
                                        direction: UIPageViewControllerNavigationDirection = .forward) {
        setViewControllers([viewController],
                           direction: direction,
                           animated: false,
                           completion: { (finished) -> Void in
                            // Setting the view controller programmatically does not fire
                            // any delegate methods, so we have to manually notify the
                            self.notifySurveyDelegateOfNewIndex()
        })
    }
    
    /**
     Notifies that the current page index was updated.
     */
    func notifySurveyDelegateOfNewIndex() {
        if let firstViewController = viewControllers?.first as? NBSurveyViewController,
            let index = orderedViewControllers.index(of: firstViewController) {
            surveysPageViewControllerDelegate?.surveysPageViewController(surveysPageViewController: self, didUpdatePageIndex: index)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func newSurveyViewController() {
        var surveysViewController: [NBSurveyViewController] = []
        for i in 0..<surveys!.count {
            let surveyViewController =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NBSurveyViewController") as! NBSurveyViewController
            surveyViewController.survey = self.surveys![i]
            surveysViewController.append(surveyViewController)
        }
        self.orderedViewControllers = surveysViewController
        if let initialViewController = orderedViewControllers?[lastIndex] {
            scrollToViewController(initialViewController)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func loadSurveysIfNeed() {
        let surveyCount = surveys?.count ?? 0
        if surveyRequest == nil {
            self.surveyRequest = NBAppGlobal.sharedInstance.service.requestForSurveyAPI(page: page, per_page: perPage)
            surveyRequest?.drawerArrayCompletionHandler(completionHandler: { (response) in
                self.surveyRequest = nil
                if response.result.isSuccess, let dataSurveys = response.result.value {
                    HUD.flash(.success)
                    if surveyCount == 0 {
                        self.lastIndex = 0
                        self.surveys = dataSurveys
                    } else {
                        self.lastIndex = surveyCount - 1
                        self.surveys?.append(contentsOf: dataSurveys)
                    }
                    self.surveysPageViewControllerDelegate?.surveysPageViewController(surveysPageViewController: self, didUpdatePageCount: self.surveys?.count ?? 0)
                    self.newSurveyViewController()
                    self.page += 1
                } else {
                    // fail
                    HUD.flash(.labeledError(title: "Error", subtitle: response.result.error?.localizedDescription))
                }
            })
            HUD.show(.progress)
            surveyRequest?.start()
        }
    }
    
}

extension NBSurveysPageViewController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        notifySurveyDelegateOfNewIndex()
    }
}

extension NBSurveysPageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let surveyViewController = viewController as? NBSurveyViewController else {
            return nil
        }
        guard let viewControllerIndex = orderedViewControllers.index(of: surveyViewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        // User is on the first view controller and swiped left to loop to
        // the last view controller.
        guard previousIndex >= 0 else {
            return orderedViewControllers.last
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let surveyViewController = viewController as? NBSurveyViewController else {
            return nil
        }
        guard let viewControllerIndex = orderedViewControllers.index(of: surveyViewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        // User is on the last view controller and swiped right to loop to
        // the first view controller.
        guard orderedViewControllersCount != nextIndex else {
            return orderedViewControllers.first
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
}
